**Passages aux urgences et actes SOS médecins liés à la Covid-19 (2019-2022)**
___

# Description 

Les données _covid_urgences_sosmedecins_ rapportent les _nombres de passages aux urgences_ et _actes SOS médecins totaux_ et pour _suspicion de Covid-19_ par _département_ et _classe d'âge_. Le _pourcentage_ d'évènements pour suspicion de covid sont également présentées ici. Enfin, on retrouve dans ces données le nombre (et le _pourcentage_) d'_hospitalisation_ à l'issue d'un passage aux urgences pour suspicion de covid.

Les données sont produites par _Santé Public France_<sup>(1)</sup> avec l'objectif de surveiller et comprendre les dynamiques de l'épidémie sur le territoire français. Il est cependant important de relever que la déclaration des cas de covid n'est pas exhaustive.

# Opérations de datamanagement réalisées

Les variables par sexe présentant chacune plus de 85% de données manquantes, elles ont été retirées du jeu de données final. 
Les colonnes contenant des identifiants n'ayant pas d'intérêt pour d'éventuelles réutilisations ont également été retirées. 

Plusieurs variables ont été renommées afin de suivre la nomenclature définie et de rendre leur nom plus explicite.

Une colonne _classe_age_ reprenant le libellé de la classe d'âge au format **"agemin-agemax"** a été ajoutée afin de faciliter le croisement avec d'autres sources de données. 

Enfin les variables suivantes ont été calculées : 

- le pourcentage de passage aux urgences pour suspicion de covid parmi l'ensemble des passages aux urgences _(rate_covid_urg)_
- le pourcentage d'hospitalisation à l'issue d'un passage aux urgences pour suspicion de covid parmi l'ensemble de ces derniers _(rate_hospi_covid_by_urg)_
- le pourcentage d'actes SOS médecins pour suspicion de covid parmi l'ensemble des actes SOS médecins réalisés _(rate_covid_sosmed)_



# Aperçus des données

### _5 premières lignes_

![](./images/head_covid_urgences_sosmedecins_2019_2022.png)


### _5 dernières lignes_

![](./images/tail_covid_urgences_sosmedecins_2019_2022.png)


# Licence et conditions de réutilisation

Licence ouvert Etalab v2.0


# Données brutes

**Accéder aux données brutes** _https://data.opendatasoft.com/explore/dataset/coronavirus-tranche-age-urgences-sosmedecins-dep-france%40public/information/?disjunctive.nom_dep_min_

# Références

(1) https://www.santepubliquefrance.fr/
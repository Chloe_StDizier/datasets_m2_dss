**Data pathologies (2015-2020)**
___

# Description 

Les données _data_pathologies_ contiennent des informations sur les effectifs de patients pris en charge par l'ensemble des régimes d'assurance maladie. Les pathologies, traitements chroniques ou épisodes de soin sont regroupés en trois niveau de détails. On retrouve pour chaque groupe ainsi formé le nombre de _patients pris en charge_ par _sexe_, _classe d'âge_ et _département_ pour les _années_ 2015 à 2020. La _prévalence_ ainsi que la _population de référence_ utilisée pour son calcul se retrouvent également dans ces données exprimées respectivement en pourcentage et nombre d'habitants.

Ces données sont produites par la *Caisse nationale de l'Assurance Maladie (Cnam)*<sup>(1)</sup> et sont utilisées pour la plateforme de datavisualisation *Data pathologies*<sup>(2)</sup>.

# Opérations de datamanagement réalisées

Plusieurs variables ont été renommées afin de suivre la nomenclature définie et de rendre leur nom plus explicite.

Les colonnes contenant des identifiants n'ayant pas d'intérêt pour d'éventuelles réutilisations ont également été retirées. 

# Aperçus des données

### _5 premières lignes_

![](./images/head_data_pathologies_2015_2020.png)


### _5 dernières lignes_

![](./images/tail_data_pathologies_2015_2020.png)


# Licence et conditions de réutilisation 

Open Database License (ODbL)


# Données brutes

**Accéder aux données brutes** _https://data.ameli.fr/explore/dataset/effectifs/information/_

# Références

(1) https://assurance-maladie.ameli.fr/
 
(2) https://data.ameli.fr/pages/data-pathologies/